from flask import Flask
from flask_user import UserManager, SQLAlchemyAdapter

from registry import db
from registry.users.models import User
from registry.organizations.models import Organization # noqa


def create_app():
    app = Flask(__name__)

    app.config.update({
        'SECRET_KEY': 'totally-insecure-key',
        'SERVER_NAME': 'localhost:5000',
        'CSRF_ENABLED': False,
        'SQLALCHEMY_TRACK_MODIFICATIONS': False,
        'SQLALCHEMY_DATABASE_URI':
            'postgresql://registry:registry@localhost/registry',
        'USER_ENABLE_CONFIRM_EMAIL': False,
        'USER_AFTER_LOGIN_ENDPOINT': 'organizations.dashboard',
        'USER_AFTER_LOGOUT_ENDPOINT': 'public.home',
        'USER_SEND_REGISTERED_EMAIL': False,
        'WTF_CSRF_ENABLED': False,
    })

    configure_extensions(app)
    register_blueprints(app)

    return app


def configure_extensions(app):
    db.init_app(app)

    UserManager(
        db_adapter=SQLAlchemyAdapter(db, User),
        app=app,
    )


def register_blueprints(app):
    from registry.public.views import public
    app.register_blueprint(public)

    from registry.organizations.views import organizations
    app.register_blueprint(organizations)
