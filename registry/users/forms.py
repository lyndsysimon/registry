from flask_user.forms import RegisterForm
from flask_wtf import FlaskForm
import wtforms as wtf


class MyRegisterForm(RegisterForm):
    first_name = wtf.StringField(
        'First name',
        validators=[wtf.validators.DataRequired('First name is required')],
    )
    last_name = wtf.StringField(
        'Last name',
        validators=[wtf.validators.DataRequired('Last name is required')],
    )


class UserProfileForm(FlaskForm):
    first_name = wtf.StringField(
        'First name',
        validators=[wtf.validators.DataRequired('First name is required')],
    )
    last_name = wtf.StringField(
        'Last name',
        validators=[wtf.validators.DataRequired('Last name is required')],
    )
    submit = wtf.SubmitField('Save')
