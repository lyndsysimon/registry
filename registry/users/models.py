from flask_login import UserMixin

from registry import db


class Role(db.Model):
    __tablename__ = 'roles'

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(
        db.String(50),
        nullable=False,
        server_default='',
        unique=True,
    )
    label = db.Column(db.String(255), server_default='')


class User(db.Model, UserMixin):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True)

    email = db.Column(
        db.String(255),
        nullable=False,
        server_default='',
        unique=True,
    )
    confirmed_at = db.Column(db.DateTime())
    password = db.Column(db.String(255), nullable=False, server_default='')
    active = db.Column(db.Boolean(), nullable=False, server_default='0')

    active = db.Column(
        'is_active',
        db.Boolean(),
        nullable=False,
        server_default='0',
    )
    first_name = db.Column(db.Unicode(50), nullable=False, server_default='')
    last_name = db.Column(db.Unicode(50), nullable=False, server_default='')

    roles = db.relationship(
        'Role',
        secondary='users_roles',
        backref=db.backref('users', lazy='dynamic'),
    )

    def __repr__(self):
        return '<User {id}: {username}>'.format(
            id=self.id,
            username=self.username,
        )


class UsersRoles(db.Model):
    __tablename__ = 'users_roles'

    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(
        db.Integer(),
        db.ForeignKey('users.id', ondelete='CASCADE'),
    )
    role_id = db.Column(
        db.Integer(),
        db.ForeignKey('roles.id', ondelete='CASCADE'),
    )
