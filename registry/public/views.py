from flask import Blueprint, render_template
from flask_user import current_user


public = Blueprint('public', __name__, template_folder='templates')


@public.route('/')
def home():
    return render_template('home.html')


@public.route('/whoami')
def whoami():
    from flask import escape
    if current_user.is_authenticated:
        return '<pre>{}</pre>'.format(escape(repr(current_user)))
    return 'You are not logged in.'
