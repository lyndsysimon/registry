from registry import db


class Organization(db.Model):
    __tablename__ = 'organizations'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship(
        'User',
        backref=db.backref('organizations', lazy='dynamic'),
    )

    def __repr__(self):
        return '<Organization {id}: {name}>'.format(
            id=self.id,
            name=self.name,
        )
