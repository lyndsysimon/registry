from flask import Blueprint, render_template
from flask_user import login_required


organizations = Blueprint(
    'organizations',
    __name__,
    template_folder='templates',
    url_prefix='/orgs',
)


@organizations.route('/')
@login_required
def dashboard():
    return render_template('dashboard.html')
