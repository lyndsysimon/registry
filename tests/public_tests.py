from nose.tools import *  # noqa

from flask import url_for

from tests.base import RegistryTestCase


class PublicTestCase(RegistryTestCase):
    def test_home(self):
        resp = self.client.get(url_for('public.home'))
        assert_equal(resp.status_code, 200)
