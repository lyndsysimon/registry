import unittest

from registry import db
from registry.core import create_app


class RegistryTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.app.app_context().push()
        self.client = self.app.test_client()
        db.drop_all(app=self.app)
        db.create_all(app=self.app)

    def tearDown(self):
        pass
