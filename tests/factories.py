import factory
from factory.alchemy import SQLAlchemyModelFactory as Factory

from registry import db
from registry.organizations.models import Organization
from registry.users.models import User


class UserFactory(Factory):
    class Meta:
        model = User
        sqlalchemy_session = db.session

    username = factory.Sequence(lambda n: 'User {}'.format(n))
    email = factory.Sequence(lambda n: 'user{}@example.com'.format(n))


class OrganizationFactory(Factory):
    class Meta:
        model = Organization
        sqlalchemy_session = db.session

    name = factory.Sequence(lambda n: 'Org {}'.format(n))
    user = factory.SubFactory(UserFactory)
