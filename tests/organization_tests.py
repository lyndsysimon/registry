from flask import url_for
from nose.tools import *  # noqa

from tests.base import RegistryTestCase


class OrganizationLoggedOutTestCase(RegistryTestCase):
    def test_dashboard(self):
        url = url_for('organizations.dashboard', _external=False)
        response = self.client.get(url)
        assert_equal(response.status_code, 302)
