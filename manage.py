from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from registry import db
from registry.core import create_app


if __name__ == '__main__':
    app = create_app()
    Migrate(app, db)
    manager = Manager(app)
    manager.add_command('db', MigrateCommand)
    manager.run()
